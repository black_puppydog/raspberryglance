#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from time import sleep, strftime
from datetime import datetime
from ResRobot import scrape_api, url

lcd = Adafruit_CharLCD()

cmd = "/home/pi/lcd/ResRobot.py"

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

while 1:
	sleep(30)
	lcd.clear()
	try:
		infos = list(scrape_api(url))[:2]
	except:
		pass
	#ipaddr = run_cmd(cmd)
	#lcd.message(datetime.now().strftime('%b %d  %H:%M:%S\n'))
	
	for (depart, arrive) in infos:
		lcd.message(' {0} -->{1}\n'.format(depart.strftime("%H:%M"), arrive.strftime("%H:%M")))
