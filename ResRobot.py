#! /usr/bin/env python2.7

datetime_format = "%Y-%m-%d %H:%M"
__author__ = 'daan'

import json
import urllib2
from datetime import datetime

api_key = "something"
from_id = "7467489" # stora lappkarberget
to_id = "7421645" # universitetet tbana

url = "https://api.trafiklab.se/samtrafiken/resrobot/Search.json?key={0}&fromId={1}&toId={2}&".format(api_key, from_id, to_id)


def extract_departure_time(ttitem):
    timestring = ttitem['segment'][0]["departure"]['datetime']
    return datetime.strptime(timestring, datetime_format)


def extract_arrival_time(ttitem):
    timestring = ttitem['segment'][0]["arrival"]['datetime']
    return datetime.strptime(timestring, datetime_format)


def extract_type(ttitem):
    return ttitem['segment'][0]["segmentid"]['mot']['#text']


def scrape_api(cmd):
    site = urllib2.urlopen(url)
    content = site.read()
    site.close()

    j = json.loads(content, encoding="ISO-8859-1")
    ttitems = j['timetableresult']["ttitem"]
    return [(extract_departure_time(item), extract_arrival_time(item))
            for item in ttitems
            if extract_type(item) == "Buss"
            and extract_departure_time(item) > datetime.now()]

if __name__=='__main__':
    infos = scrape_api(url)
    for (depart, arrive) in infos:
        print('{0} --> {1}'.format(depart.strftime("%H:%M"), arrive.strftime("%H:%M")))
